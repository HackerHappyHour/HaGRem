# HaGRem Questions, and Requirements

## Questions HaGRem is designed to answer

- Why aren't there more Github clones?
- Why aren't there any github-like tools written in JavaScript? 

## Requirements for the development of HaGREM

As long as we're trying to answer the primary questions, we've decided to see if we
can resolve some shortcomings of existing git remote clients

- avoid the use of slow technologies/platforms (like Ruby/Rails)
- designed to be self-hosted from the start
- designed to be scalable from the start
- use backend technologies that are designed to be natively scaled horizontally (without 3rd party services)

## HaGRem Goals

The combination of the Questions and Requirements should allow us to reach the following goals

- Have a Git Remote Client that is very performant even under high load
- Have a Scalable Git Remote Client that is _very_ easy to deploy
